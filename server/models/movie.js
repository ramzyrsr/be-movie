const mongoose = require("mongoose");

const UserSchema = mongoose.Schema(
    {
        title: {
            type: String,
            required: true
        },
        rating: {
            type: BigInt,
            required: true
        },
        genre: {
            type: String,
            required: true
        },
        synopsis: {
            type: String,
            required: true
        },
        review: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user',
        }],
        img: {
            type: String,
            data: Buffer
        }
    },
    {
        timestamps: true
    }
);



module.exports = mongoose.model('movie', UserSchema);