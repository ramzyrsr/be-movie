const { movie } = require('../models');

exports.getReviews = async (req, res, next) => {
    try {
        const getMovie = await movie.find({ title: req.params.title });

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Get all review',
            data: getMovie
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        return res
                    .json({
                        statusCode: 500,
                        statusText: 'error',
                        message: error.message
                    })
                    .status(500);
    }
};