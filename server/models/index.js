const mongoose = require('mongoose');

mongoose
    .connect(process.env.MONGO_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: true,
        useCreateIndex: true
    })
    .then(() => console.log('MongoDB Connected'))
    .catch((error) => console.log(error.message));

const movie = require('./movie');

module.exports = { movie };