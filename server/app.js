require('dotenv').config();

const express = require('express');
const cors = require('cors');

// Route variable

const port = process.env.PORT || 8080;
const app = express();

app.use(express.json());
app.use(
    express.urlencoded({
        extended: false
    })
);
app.use(cors());

// routing

app.all('*', (req, res) =>
    res.status(404).send(`Can't find ${req.originalUrl} on this server!`)
);

app.listen(port, () => console.log(`server running on port ${port}`));